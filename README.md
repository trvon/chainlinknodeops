# Chainlink

## [Getting Started with Deploying Node](https://blog.chain.link/what-is-a-chainlink-node-operator/)
- [Setting up a Node](https://docs.chain.link/docs/running-a-chainlink-node)
- [Deploying Oracle](https://docs.chain.link/docs/fulfilling-requests#config)

## Adapters
- [Core Chainlink Adapters](https://docs.chain.link/docs/adapters)
- [External Adapter JS](https://github.com/smartcontractkit/external-adapters-js)

## Smart Contract Deployment
- [Remix](https://remix.ethereum.org)

## Testnet Resources
- [Chainlink Faucet](https://kovan.chain.link)
- [KETH Faucet](https://faucet.kovan.network)

### Additional Reading
- [Layer 2 Scaling](https://ethereum.org/en/developers/docs/layer-2-scaling/)

### Useful Scripts
- Allow docker to communicate with local postgresql

```
iptables -A INPUT -i docker0 -j ACCEPT
```


- Start Chainlink

``` 
if [ $(docker ps | grep geth | wc -l) == 0 ]; then
    echo "Starting geth..."
    docker run --name eth -it -d -p 8546:8546  -v ~/.ethereum:/root/.ethereum  ethereum/client-go:latest --ws --ipcdisable --ws.addr 0.0.0.0 --ws.origins="*" --rinkeby --datadir /chaindata
fi

sleep 10

cd ~/.chainlink-kovan && docker run -p 6688:6688 -v ~/.chainlink-kovan:/chainlink -it --env-file=.env smartcontract/chainlink:latest local n
```
