#!/bin/bash

if [ $(docker ps | grep geth | wc -l) == 0 ]; then
		    echo "Starting geth..."
			    docker run --name eth -it -d -p 8546:8546  -v ~/.ethereum:/root/.ethereum  ethereum/client-go:latest --ws --ipcdisable --ws.addr 0.0.0.0 --ws.origins="*" --rinkeby --datadir /chaindata
				    # docker run -d -v /chaindata:/chaindata islishude/geth --datadir=/chaindata
fi

sleep 10

# while [ $(curl -s http://127.0.0.1:8546 | wc -c) == 0 ]
# do
#     sleep 100
#     echo "Testing now.."
# done

cd ~/.chainlink-kovan && docker run -p 6688:6688 -v ~/.chainlink-kovan:/chainlink -it --env-file=.env smartcontract/chainlink:latest local n
